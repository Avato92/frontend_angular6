# ![Angular 6 Example App](logo.png)

# Presentation


Fronted project for the subject of server in the DAW cycle by Alejandro Vañó Tomás

### Making requests to the backend API

If you want to change the API URL to a local server, simply edit `src/environments/environment.ts` and change `api_url` to the local server's URL (i.e. `localhost:3000/api`)


# Getting started

Make sure you have the [Angular CLI](https://github.com/angular/angular-cli#installation) installed globally. We use [Yarn](https://yarnpkg.com) to manage the dependencies, so we strongly recommend you to use it. you can install it from [Here](https://yarnpkg.com/en/docs/install), then run `yarn install` to resolve all dependencies (might take a minute).

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Building the project
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


**General functionality:**

- Authenticate users via JWT (login/signup pages + logout button on settings page)
- CR** users
- CRU* players
- GET and display paginated lists of players and him guild

**The general page breakdown looks like this:**

- Home page 
    - List of players and guild name
    - Pagination with an algorithm to know the ranking
- Sign in/Sign up pages (URL: /#/join, /#/enroll )
    - Uses JWT (store the token in localStorage)
    - Authentication can be easily switched to session/cookie based
    - Auth configuration changed, changing the users table by the players table
    - Validation, adding differents rules depending on whether it is join or enroll
- Contact (URL: /#/contact )
    - With validation
    - Toastr
    - Error-directive

<br />

