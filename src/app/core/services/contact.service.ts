import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Contact } from '../models';
import { map } from 'rxjs/operators';

@Injectable()
export class ContactService {
  constructor (
    private apiService: ApiService
  ) {}
  sendEmail(contact): Observable<Contact> {
    return this.apiService
    .post('/contact' , {contact: contact})
    .pipe(map(data => {
      return data;
      }));
      
    }
}