import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable ,  BehaviorSubject ,  ReplaySubject } from 'rxjs';

import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { Players } from '../models';
import { map ,  distinctUntilChanged } from 'rxjs/operators';

@Injectable()
export class EnrollJoinService {
  private currentPlayerSubject = new BehaviorSubject<Players>({} as Players);
  public currentPlayer = this.currentPlayerSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor (
    private apiService: ApiService,
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

    // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.
  populate() {
    // If JWT detected, attempt to get & store user's info
    if (this.jwtService.getToken()) {
      this.apiService.get('/user')
      .subscribe(
        data => this.setAuth(data.players),
        err => this.purgeAuth()
      );
    } else {
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
    }
  }

  setAuth(player: Players) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(player.token);
    // Set current user data into observable
    this.currentPlayerSubject.next(player);
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    // Set current user to an empty object
    this.currentPlayerSubject.next({} as Players);
    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
  }

  attemptAuth(type, credentials): Observable<Players> {
    if(type === 'join'){
      credentials.type = 'join';
    }else{
      credentials.type = 'enroll';
    }
    // return this.apiService.post('/players' + route, {player: credentials})
    return this.apiService.post('/players', {player: credentials})
      .pipe(map(
      data => {
        this.setAuth(data.players);
        return data;
      }
    ));
  }
  getCurrentPlayer(): Players {
    return this.currentPlayerSubject.value;
  }

}