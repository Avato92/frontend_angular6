import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Players, PlayersListConfig } from '../models';
import { map } from 'rxjs/operators';

@Injectable()
export class PlayersService {
  constructor (
    private apiService: ApiService
  ) {}
 	query(config: PlayersListConfig): Observable<{players : Players[], playersCount: number}>{
    const params = {};

     Object.keys(config.filters)
     .forEach((key) => {
       params[key] = config.filters[key];
     });
 
     return this.apiService
     .get(
       '/players' + ((config.type === 'feed') ? '/feed' : ''),
       new HttpParams({ fromObject: params })
     );
   }
   get(slug): Observable<Players> {
    return this.apiService.get('/players/' + slug)
      .pipe(map(data => data.players));
  }
  };
