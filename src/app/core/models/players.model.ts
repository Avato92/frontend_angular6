export interface Players{
	slug: string,
	playerName: string,
	lvl: number,
	honor: number,
	gold: number,
	str: number,
	dex: number,
	int: number,
	con: number,
	guild: number,
	job: number,
	photo: string,
	class: string,
	email: string,
	token: string
}