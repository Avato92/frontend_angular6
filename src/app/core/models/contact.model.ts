export interface Contact {
    name: string;
    email: string;
    reason: string;
  }
  