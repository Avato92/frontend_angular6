export interface PlayersListConfig{
	type: string;

	filters:{
		slug?: string,
		guild?: string,
		limit?: number,
		offset?: number
	};
}