import { Component, OnInit } from '@angular/core';

import { Players, EnrollJoinService } from '../../core';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  constructor(
    private EnrollJoinService: EnrollJoinService
  ) {}

  currentPlayer: Players;

  ngOnInit() {
    this.EnrollJoinService.currentPlayer.subscribe(
      (playerData) => {
        this.currentPlayer = playerData;
      }
    );
  }
}
