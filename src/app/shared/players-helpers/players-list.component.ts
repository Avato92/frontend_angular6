import { Component, Input } from '@angular/core';

import { Players, PlayersListConfig, PlayersService } from '../../core';
@Component({
  selector: 'app-players-list',
  styleUrls: ['players-list.component.css'],
  templateUrl: './players-list.component.html'
})
export class PlayersListComponent {
  constructor (
    private playersService: PlayersService
  ) {}

  @Input() limit: number;
  @Input()
  set config(config: PlayersListConfig) {
    if (config) {
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  query: PlayersListConfig;
  results: Players[];
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [1];

  setPageTo(pageNumber) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  runQuery() {
    this.loading = true;
    this.results = [];

    // Create limit and offset filter (if necessary)
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset =  (this.limit * (this.currentPage - 1));
    }

    this.playersService.query(this.query)
    .subscribe(data => {
      this.loading = false;
      this.results = data.players;


       this.totalPages = Array.from(new Array(Math.ceil(data.playersCount / this.limit)), (val, index) => index + 1); 
    });
  }
}