import { Component, OnInit } from '@angular/core';

import { EnrollJoinService } from './core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor (
    private EnrollJoinService: EnrollJoinService
  ) {}

  ngOnInit() {
    this.EnrollJoinService.populate();
  }
}
