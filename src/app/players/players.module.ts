import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PlayerResolver } from './players-resolver.service';
import { PlayersComponent } from './players.component';
import { SharedModule } from '../shared';
import { PlayersRoutingModule } from './players-routing.module';
import { PlayersDetailsComponent } from './players-details.component';

@NgModule({
  imports: [
    SharedModule,
    PlayersRoutingModule
  ],
  declarations: [
    PlayersDetailsComponent,
    PlayersComponent
  ],
  providers: [PlayerResolver]
})
export class PlayersModule {}