import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { Players, PlayersService } from '../core';
import { catchError ,  map } from 'rxjs/operators';

@Injectable()
export class PlayerResolver implements Resolve<Players> {
  constructor(
    private playersService: PlayersService,
    private router: Router,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {

    return this.playersService.get(route.params['slug'])
      .pipe(
        map(
          players => {
            console.log(players);
              return players;
          }
        ),
        catchError((err) => this.router.navigateByUrl('/'))
      );
  }
}