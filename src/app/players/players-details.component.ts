import { Component, OnInit } from '@angular/core';
import { Players } from '../core';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-details-players-page',
    templateUrl: './players-details.component.html',
    styleUrls: ['./players-details.component.css']
  })
  export class PlayersDetailsComponent implements OnInit {
    players: Players;


    constructor(
      private route: ActivatedRoute
      ) {}


    ngOnInit(){
      
      this.route.data.subscribe(
        (data: { player : Players }) => {
          this.players = data.player;
          
          if(this.players.photo == null ){
            if(this.players.job == 1){
              this.players.photo = "assets/guerrero.jpg";
            }else if(this.players.job == 2){
              this.players.photo = "assets/arquero.jpg";
            }else{
              this.players.photo = "assets/mago.jpg";
            }
          }
          if(this.players.job == 1){
            this.players.class = "warrior";
          }else if(this.players.job == 2){
            this.players.class = "archer";
          }else{
            this.players.class = "wizard";
          }
        }
      );
    }
  }