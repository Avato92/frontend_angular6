import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayersComponent } from './players.component';
import { PlayersDetailsComponent } from './players-details.component';
import { PlayerResolver } from './players-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: PlayersComponent
  },
  {
    path: 'players/:slug',
    component: PlayersDetailsComponent,
    resolve: {
      player: PlayerResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayersRoutingModule {}