import { Component, OnInit } from '@angular/core';
import { Players, PlayersListConfig } from '../core';


@Component({
    selector: 'app-players-page',
    templateUrl: './players.component.html'
  })
  export class PlayersComponent implements OnInit {
    players: Players;

    constructor(
      ) {}

    listConfig: PlayersListConfig = {
      type: 'all',
      filters:{}
    };

    ngOnInit(){
      this.setListTo('all');
    }

    setListTo(type: string = '', filters: Object = {}) {
      this.listConfig = {type: type, filters: filters};
    }
    
  }