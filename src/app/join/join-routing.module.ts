import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JoinComponent } from './join.component';
import { NoJoin } from './no-join.service';

const routes: Routes = [
  {
    path: 'join',
    component: JoinComponent,
    canActivate: [NoJoin]
  },
  {
    path: 'enroll',
    component: JoinComponent,
    canActivate: [NoJoin]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoinRoutingModule {}