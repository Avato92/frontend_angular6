import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { EnrollJoinService } from '../core';
import { map ,  take } from 'rxjs/operators';

@Injectable()
export class NoJoin implements CanActivate {
  constructor(
    private router: Router,
    private joinService: EnrollJoinService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {

    return this.joinService.isAuthenticated.pipe(take(1), map(isAuth => !isAuth));

  }
}
