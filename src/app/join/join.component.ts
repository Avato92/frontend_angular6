import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Errors, EnrollJoinService } from '../core';

@Component({
  selector: 'app-join-page',
  templateUrl: './join.component.html'
})
export class JoinComponent implements OnInit {
  joinType: String = '';
  title: String = '';
  errors: Errors = {errors: {}};
  isSubmitting = false;
  joinForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private EnrollJoinService: EnrollJoinService,
    private fb: FormBuilder
  ) {
    // use FormBuilder to create a form group
    this.joinForm = this.fb.group({
      'email': ['', [Validators.required, Validators.email]],
      'password': ['', [Validators.required, Validators.minLength(6)]]
    });

    
  }

  ngOnInit() {
    this.route.url.subscribe(data => {  
      // Get the last piece of the URL (it's either 'login' or 'register')
      this.joinType = data[data.length - 1].path;
      // Set a title for the page accordingly
      this.title = (this.joinType === 'join') ? 'Join' : 'Enroll';
      // add form control for username if this is the register page
      if (this.joinType === 'enroll') {
        this.joinForm.addControl('name', new FormControl('', Validators.required));
        this.joinForm.addControl('class', new FormControl(''));
        this.joinForm.get('class').setValue('1');
      }
    });
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = {errors: {}};

    const credentials = this.joinForm.value;
    this.EnrollJoinService
    .attemptAuth(this.joinType, credentials)
    .subscribe(
      data => this.router.navigateByUrl('/'),
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
