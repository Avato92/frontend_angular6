import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JoinComponent } from './join.component';
import { NoJoin } from './no-join.service';
import { SharedModule } from '../shared';
import { JoinRoutingModule } from './join-routing.module';

@NgModule({
  imports: [
    SharedModule,
    JoinRoutingModule
  ],
  declarations: [
    JoinComponent
  ],
  providers: [
    NoJoin
  ]
})
export class JoinModule {}