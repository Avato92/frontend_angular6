import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from '../core';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
    selector: 'app-contact-page',
    templateUrl: './contact.component.html'
  })
  export class ContactComponent{

    contactForm: FormGroup;
    errors: Object = {};
    isSubmitting = false;

    constructor(
        private contactService: ContactService,
        private fb: FormBuilder,
        public toastr: ToastrManager
      ) {
        this.contactForm = this.fb.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            reason: ['', [Validators.required, Validators.minLength(20)]]
          });
        }
        submitForm(){
          this.errors = "";
          this.isSubmitting = true;
          this.contactService
          .sendEmail(this.contactForm.value)
          .subscribe(
            data => {
            this.toastr.successToastr('Your mail has been sent.', 'Success!');
            console.log(data);
          },
            err => {
              console.log(err);
              this.errors = err;
              this.isSubmitting = false;
              this.toastr.errorToastr('There was a problem sending your mail.', 'Oops!');
          }
      );
    }
  }