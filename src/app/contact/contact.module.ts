import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContactComponent } from './contact.component';
import { ContactRoutingModule } from './contact-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared';

@NgModule({
    imports: [ContactRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      SharedModule
    ],
    declarations: [ContactComponent]
  })
  export class ContactModule {}